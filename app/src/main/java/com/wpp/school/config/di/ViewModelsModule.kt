package com.wpp.school.config.di

import com.wpp.school.ui.enjoy.EnjoyViewModel
import com.wpp.school.ui.introduce.CollegeIntroViewModel
import com.wpp.school.ui.main.home.HomeViewModel
import com.wpp.school.ui.news.NewsViewModel
import com.wpp.school.ui.practice.PracticeViewModel
import com.wpp.school.ui.timetable.TimetableViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * viewModel模块
 */
val viewModelsModule = module {
    viewModel { CollegeIntroViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { PracticeViewModel(get()) }
    viewModel { NewsViewModel(get()) }
    viewModel { EnjoyViewModel(get()) }
    viewModel { TimetableViewModel(get()) }
}