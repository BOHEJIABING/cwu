package com.wpp.school.config.di

import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.repository.CwuRepository
import org.koin.dsl.module

/**
 * 接口实现模块
 */
val repositoryModule = module {
    fun provideAppRepository(
        api: CwuApi
    ) : ICwuRepository {
        return CwuRepository(api)
    }

    single { provideAppRepository(get()) }
}
