package com.wpp.school.config.di

import com.wpp.school.data.api.CwuApi
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * 接口模块
 */
val apiModule = module {

    single { provideCwuApi(get()) }

}

internal fun provideCwuApi(retrofit: Retrofit): CwuApi =
    retrofit.create(CwuApi::class.java)