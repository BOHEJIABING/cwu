package com.wpp.school.data.repository

import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg

/**
 * @desc  接口实现类
 * @author peipei.wu
 * @created 2021/10/2 12:46
 */
class CwuRepository(cwuApi: CwuApi) : ICwuRepository {

    private val introduceRepository = IntroduceRepository(cwuApi)
    private val homeRepository      = HomeRepository(cwuApi)
    private val practiceRepository  = PracticeRepository(cwuApi)
    private val newsRepository      = NewsRepository(cwuApi)
    private val enjoyRepository     = EnjoyRepository(cwuApi)
    private val timetableRepository = TimetableRepository(cwuApi)


    override suspend fun getMainRecommend(): DataResult<MutableList<BaseImg>> {
        return homeRepository.getMainRecommend()
    }

    override suspend fun getPractice(): DataResult<MutableList<BaseImg>> {
        return practiceRepository.getPractice()
    }

    override suspend fun getNews(): DataResult<MutableList<BaseImg>> {
        return newsRepository.getNews()
    }

    override suspend fun getEnjoyBackground(): DataResult<BaseConfig> {
        return enjoyRepository.getEnjoyBackground()
    }

    override suspend fun getEnjoyStu(): DataResult<MutableList<BaseImg>> {
        return enjoyRepository.getEnjoyStu()
    }

    override suspend fun getEnjoyTeacher(): DataResult<MutableList<BaseImg>> {
        return enjoyRepository.getEnjoyTeacher()
    }

    override suspend fun getAllCondition(): DataResult<MutableList<BaseConfig>> {
        return timetableRepository.getAllCondition()
    }

    override suspend fun getTimetableByCId(conditionId: Int): DataResult<MutableList<BaseImg>> {
        return timetableRepository.getTimetableByCId(conditionId)
    }

    override suspend fun getIntroduce(): DataResult<BaseConfig> {
        return introduceRepository.getIntroduce()
    }

    override suspend fun getTimeToHome(): DataResult<String> {
        return homeRepository.getTimeToHome()
    }

    override suspend fun getLoopTime(): DataResult<String> {
        return homeRepository.getLoopTime()
    }
}