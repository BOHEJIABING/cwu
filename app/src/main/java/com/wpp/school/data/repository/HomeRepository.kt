package com.wpp.school.data.repository

import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.api.DataSource
import com.wpp.school.data.entity.base.BaseImg

class HomeRepository(private val cwuApi: CwuApi) {

    /**
     * 获取首页推荐
     */
    suspend fun getMainRecommend(): DataResult<MutableList<BaseImg>> {
        return DataSource.getDataResult { cwuApi.getMainRecommend() }
    }

    /**
     * 获取无操作返回主页时间
     */
    suspend fun getTimeToHome() : DataResult<String> {
        return DataSource.getDataResult { cwuApi.getTimeToHome() }
    }

    /**
     * 获取轮播图轮播时间
     */
    suspend fun getLoopTime() : DataResult<String> {
        return DataSource.getDataResult { cwuApi.getLoopTime() }
    }
}