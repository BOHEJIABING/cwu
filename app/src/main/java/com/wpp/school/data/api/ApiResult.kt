package com.wpp.school.data.api

import com.google.gson.annotations.SerializedName

/**
 * 接口返回类
 */
data class ApiResult<T>(
    @SerializedName(value = "code", alternate = ["return_code"])
    val code: Int = 0,
    @SerializedName(value = "msg", alternate = ["return_msg"])
    val message: String = "",
    @SerializedName(value = "result")
    val data: T? = null
) {
    val isSuccessful: Boolean
        get() = code == 200

    val requireData: T
        get() = data!!

}