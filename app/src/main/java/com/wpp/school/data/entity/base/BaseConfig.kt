package com.wpp.school.data.entity.base

/**
 * 基础设置类
 */
open class BaseConfig(
    val mainConfig: String, val spareConfig: String?,
    val id: Int? = null, val module: Int?
)
