package com.wpp.school.data.entity.node

import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.chad.library.adapter.base.entity.node.BaseNode

/**
 * 第一个节点FirstNode，里面放子节点SecondNode
 */
open class FirstNode(
    childNode: List<BaseNode>?,
    val originImg: Int,
    val replaceImg: Int,
    method: () -> Unit
) : BaseExpandNode() {

    override val childNode: MutableList<BaseNode>? = childNode as MutableList<BaseNode>?
    val method: () -> Unit

    init {
        isExpanded = false
        this.method = method
    }

}