package com.wpp.school.data.api

import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * @desc api接口
 * @author peipei.wu
 * @created 2021/9/20 19:52
 */
interface CwuApi {

    @POST("/showImg/getMainRecommend")
    suspend fun getMainRecommend(): ApiResult<MutableList<BaseImg>>

    @POST("/showImg/getPractice")
    suspend fun getPractice(): ApiResult<MutableList<BaseImg>>

    @POST("/showImg/getNews")
    suspend fun getNews(): ApiResult<MutableList<BaseImg>>

    @POST("/config/getEnjoyBackground")
    suspend fun getEnjoyBackground(): ApiResult<BaseConfig>

    @POST("/showImg/getEnjoyStu")
    suspend fun getEnjoyStu(): ApiResult<MutableList<BaseImg>>

    @POST("/showImg/getEnjoyTeacher")
    suspend fun getEnjoyTeacher(): ApiResult<MutableList<BaseImg>>

    @POST("/condition/getAllCondition")
    suspend fun getAllCondition(): ApiResult<MutableList<BaseConfig>>

    @POST("/timetable/getTimetableByCId")
    suspend fun getTimetableByCId(@Query("conditionId") conditionId: Int): ApiResult<MutableList<BaseImg>>

    @POST("/config/getIntroduce")
    suspend fun getIntroduce(): ApiResult<BaseConfig>

    @POST("/config/getTimeToHome")
    suspend fun getTimeToHome(): ApiResult<String>

    @POST("/config/getLoopTime")
    suspend fun getLoopTime(): ApiResult<String>
}