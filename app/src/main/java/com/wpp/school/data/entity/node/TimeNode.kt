package com.wpp.school.data.entity.node

import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.chad.library.adapter.base.entity.node.BaseNode
import com.wpp.school.data.entity.base.BaseImg

/**
 * 课表节点
 */
class TimeNode(
    override val childNode: MutableList<BaseNode>?,
    val baseImg: BaseImg): BaseExpandNode() {

    init {
        isExpanded = false
    }
}