package com.wpp.school.data.api

import timber.log.Timber

/**
 * 数据源
 */
object DataSource {
    suspend fun <T> getDataResult(call: suspend () -> ApiResult<T>): DataResult<T> {
        try {
            val result = call()
            if (result.code == 200) {
                return DataResult.success(result.data)
            }
            val message = result.message
            Timber.tag("--http--").e(message)
            return error(message, result.code)
        } catch (e: Exception) {
            Timber.tag("--http--").e(e)
            return error(e.toString(), null)
        }
    }

    private fun <T> error(message: String, code: Int? = null): DataResult<T> {
        return DataResult.error(message, null, code)
    }
}
