package com.wpp.school.data.entity.node

import com.chad.library.adapter.base.entity.node.BaseNode

/**
 * 第二个节点SecondNode，没有子节点时childNode填为null即可
 */
class SecondNode(
    childNode: List<BaseNode>?,
    originImg: Int,
    replaceImg: Int,
    val methodBack: (() -> Unit?)? = null,
    method: () -> Unit
) : FirstNode(childNode, originImg, replaceImg, method)