package com.wpp.school.data.kv

import com.tencent.mmkv.MMKV
import timber.log.Timber

/**
 * kv工具类
 */
class CwuKv {
    companion object {
        private val kv = MMKV.defaultMMKV()
        private const val LOOP_TIME = "loop_time"
        private const val TIME_TO_HOME = "time_to_home"
        private const val THIS_FRAGMENT = "this_fragment"
        private const val IS_VIDEO_PLAY = "is_video_play"

        // 循环间隔时间
        var loopTime: Long
            set(value) {
                kv.encode(LOOP_TIME, value)
            }
            get() = kv.decodeLong(LOOP_TIME, 3000)

        // 返回主页时间
        var timeToHome: Long
            set(value) {
                kv.encode(TIME_TO_HOME, value)
            }
            get() = kv.decodeLong(TIME_TO_HOME, 5000)

        // 当前fragment
        var thisFragment: String?
            set(value) {
                kv.encode(THIS_FRAGMENT, value)
            }
            get() = kv.decodeString(THIS_FRAGMENT, "HomeFragment")

        // 视频是否处于播放状态
        var isVideoPlay: Boolean
            set(value) {
                kv.encode(IS_VIDEO_PLAY, value)
            }
            get() = kv.decodeBool(IS_VIDEO_PLAY, false)

        // 检查当前是否位于主页
        fun checkIsHome(): Boolean {
            if (thisFragment.equals("HomeFragment")) {
                return true
            }
            return false
        }
    }

}