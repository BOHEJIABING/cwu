package com.wpp.school.data.repository

import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.api.DataSource
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg

/**
 * 层级接口实现类
 */
class TimetableRepository(private val cwuApi: CwuApi)  {

    /**
     * 获取所有层级
     */
    suspend fun getAllCondition(): DataResult<MutableList<BaseConfig>> {
        return DataSource.getDataResult { cwuApi.getAllCondition() }
    }

    /**
     * 根据层级id获取课表
     */
    suspend fun getTimetableByCId(conditionId: Int): DataResult<MutableList<BaseImg>> {
        return DataSource.getDataResult { cwuApi.getTimetableByCId(conditionId) }
    }

}