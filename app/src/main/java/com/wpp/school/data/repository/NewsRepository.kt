package com.wpp.school.data.repository

import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.api.DataSource
import com.wpp.school.data.entity.base.BaseImg

class NewsRepository(private val cwuApi: CwuApi) {
    /**
     * 获取新闻动态
     */
    suspend fun getNews(): DataResult<MutableList<BaseImg>> {
        return DataSource.getDataResult { cwuApi.getNews() }
    }
}