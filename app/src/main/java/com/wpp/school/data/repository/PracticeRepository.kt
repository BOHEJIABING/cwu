package com.wpp.school.data.repository

import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.api.DataSource
import com.wpp.school.data.entity.base.BaseImg

class PracticeRepository(private val cwuApi: CwuApi) {
    /**
     * 获取教学实践
     */
    suspend fun getPractice(): DataResult<MutableList<BaseImg>> {
        return DataSource.getDataResult { cwuApi.getPractice() }
    }

}