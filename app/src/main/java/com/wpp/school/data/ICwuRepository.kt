package com.wpp.school.data

import com.wpp.school.data.api.DataResult
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg

interface ICwuRepository {

    /**
     * 获取首页推荐列表
     */
    suspend fun getMainRecommend(): DataResult<MutableList<BaseImg>>

    /**
     * 获取教学实践列表
     */
    suspend fun getPractice(): DataResult<MutableList<BaseImg>>

    /**
     * 获取新闻动态列表
     */
    suspend fun getNews(): DataResult<MutableList<BaseImg>>

    /**
     * 获取师生活动背景
     */
    suspend fun getEnjoyBackground(): DataResult<BaseConfig>

    /**
     * 获取学生活动列表
     */
    suspend fun getEnjoyStu(): DataResult<MutableList<BaseImg>>

    /**
     * 获取教师活动列表
     */
    suspend fun getEnjoyTeacher(): DataResult<MutableList<BaseImg>>

    /**
     * 获取所有层级
     */
    suspend fun getAllCondition(): DataResult<MutableList<BaseConfig>>

    /**
     * 根据层级id获取课表
     */
    suspend fun getTimetableByCId(conditionId: Int): DataResult<MutableList<BaseImg>>

    /**
     * 获取学院简介
     */
    suspend fun getIntroduce(): DataResult<BaseConfig>

    /**
     * 获取无操作多长时间后返回主页
     */
    suspend fun getTimeToHome(): DataResult<String>

    /**
     * 获取轮播间隔时间
     */
    suspend fun getLoopTime(): DataResult<String>
}