package com.wpp.school.data.entity.base

import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.chad.library.adapter.base.entity.node.BaseNode

/**
 * 基础图片类，可展开
 */
data class BaseImg(val url: String, val otherInfo: String) : BaseExpandNode() {
    override val childNode: MutableList<BaseNode>? = null
}