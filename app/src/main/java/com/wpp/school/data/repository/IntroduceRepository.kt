package com.wpp.school.data.repository

import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.api.DataSource
import com.wpp.school.data.entity.base.BaseConfig

class IntroduceRepository(private val cwuApi: CwuApi) {
    /**
     * 获取学院简介
     */
    suspend fun getIntroduce() : DataResult<BaseConfig> {
        return DataSource.getDataResult { cwuApi.getIntroduce() }
    }
}