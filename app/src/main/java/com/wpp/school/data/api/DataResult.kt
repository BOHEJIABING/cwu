package com.wpp.school.data.api

/**
 * 数据类
 */
data class DataResult<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    var code: Int? = 200,
    var isCache: Boolean = false,
) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(data: T?): DataResult<T> {
            return DataResult(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String?, data: T? = null, code: Int? = null): DataResult<T> {
            return DataResult(Status.ERROR, data, message, code)
        }

        fun <T> loading(data: T? = null): DataResult<T> {
            return DataResult(Status.LOADING, data, null)
        }
    }

    fun isSuccess(): Boolean {
        return status == Status.SUCCESS
    }

}