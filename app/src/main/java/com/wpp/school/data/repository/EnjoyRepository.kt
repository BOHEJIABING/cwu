package com.wpp.school.data.repository

import com.wpp.school.data.api.CwuApi
import com.wpp.school.data.api.DataResult
import com.wpp.school.data.api.DataSource
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg

class EnjoyRepository(private val cwuApi: CwuApi) {

    /**
     * 获取师生活动页面背景图
     */
    suspend fun getEnjoyBackground(): DataResult<BaseConfig> {
        return DataSource.getDataResult { cwuApi.getEnjoyBackground() }
    }

    /**
     * 获取学生活动
     */
    suspend fun getEnjoyStu(): DataResult<MutableList<BaseImg>> {
        return DataSource.getDataResult { cwuApi.getEnjoyStu() }
    }

    /**
     * 获取教师活动
     */
    suspend fun getEnjoyTeacher(): DataResult<MutableList<BaseImg>> {
        return DataSource.getDataResult { cwuApi.getEnjoyTeacher() }
    }
}