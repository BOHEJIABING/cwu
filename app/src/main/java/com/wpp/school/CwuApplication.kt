package com.wpp.school

import android.app.Application
import com.tencent.mmkv.MMKV
import com.wpp.school.config.di.apiModule
import com.wpp.school.config.di.netModule
import com.wpp.school.config.di.repositoryModule
import com.wpp.school.config.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber


class CwuApplication : Application() {

    companion object {

        private var cwuApplication: CwuApplication? = null

        fun getInstance(): CwuApplication? {
            return cwuApplication
        }
    }

    override fun onCreate() {
        super.onCreate()

        cwuApplication = this

        // 初始化Timber
        Timber.plant(Timber.DebugTree())

        // 初始化mmkv
        MMKV.initialize(this)

        // start koin
        startKoin {
            androidContext(this@CwuApplication)
            modules(
                viewModelsModule,
                repositoryModule,
                apiModule,
                netModule
            )
        }
    }


}