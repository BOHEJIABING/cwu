package com.wpp.school.router

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.wpp.school.R
import com.wpp.school.ui.base.BaseDetailFragmentArgs
import com.wpp.school.ui.enjoy.EnjoyFragmentArgs
import com.wpp.school.ui.main.MainActivity
import com.wpp.school.ui.main.MainActivity.Companion.JUMP_BANNER
import com.wpp.school.ui.main.MainActivity.Companion.KEY_JUMP_ACTION
import com.wpp.school.ui.timetable.TimetableFragmentArgs
import timber.log.Timber

/**
 * @desc  跳转路由器类
 * @author peipei.wu
 * @created 2021/9/20 19:35
 */
class CwuRouter {
    /**
     * 主页
     */
    object Main {
        // 推荐列表
        fun homeRecommend(fragment: Fragment) {
            fragment.findNavController().navigate(R.id.home)
        }
        // 推荐列表
        fun homeRecommend(context: Context) {
            val intent = mainActivityIntent(context)
            intent.putExtra(KEY_JUMP_ACTION, JUMP_BANNER)
            context.startActivity(intent)
        }

        private fun mainActivityIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            if (context !is Activity) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            return intent
        }

        // 详情页
        fun baseDetail(fragment: Fragment, detailUrl: String) {
            Timber.d("detail%s-router: ", detailUrl)
            fragment.findNavController()
                .navigate(R.id.baseDetail, BaseDetailFragmentArgs(detailUrl).toBundle())
        }
    }

    /**
     * 学院简介
     */
    object Intro {
        // 学院简介
        fun collegeIntro(fragment: Fragment) {
            fragment.findNavController().navigate(R.id.intro)
        }
    }

    /**
     * 新闻动态
     */
    object News {
        //  新闻动态
        fun collegeNews(fragment: Fragment) {
            fragment.findNavController().navigate(R.id.news)
        }
    }

    /**
     * 教学实践
     */
    object Practice {
        // 教学实践
        fun teachPractice(fragment: Fragment) {
            fragment.findNavController().navigate(R.id.practice)
        }
    }

    /**
     * 课表
     */
    object Timetable {
        // 具体课表模块
        fun selectTimetable(fragment: Fragment, whoSelect: Int) {
            fragment.findNavController()
                .navigate(R.id.picker, TimetableFragmentArgs(whoSelect.toString()).toBundle())
        }
    }

    /**
     * 师生活动
     */
    object Enjoy {
        // 具体模块
        fun enjoy(fragment: Fragment, whoSelect: Int) {
            fragment.findNavController()
                .navigate(R.id.enjoy, EnjoyFragmentArgs(whoSelect.toString()).toBundle())
        }
    }
}