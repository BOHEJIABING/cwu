package com.wpp.school.util

import android.app.Activity
import android.graphics.Color
import android.view.View
import android.view.WindowManager

// 状态栏工具类
object StatusBarUtil {
    // 透明状态栏
    fun setTransparent(activity: Activity) {
        activity.window.apply {
            // 设置 FLAG_TRANSLUCENT_STATUS 会导致出现半透明的黑色遮罩效果，因此去除该标志
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            // SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN：将页面内容延伸到状态栏
            // SYSTEM_UI_FLAG_LAYOUT_STABLE：用于稳定布局，当状态栏发生动态显示和隐藏时，
            // 系统为 fitSystemWindow=true 的 View 设置的 padding 大小都不会变化，
            // 所以 View 的内容的位置也不会发生移动
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = Color.TRANSPARENT
        }
    }
}