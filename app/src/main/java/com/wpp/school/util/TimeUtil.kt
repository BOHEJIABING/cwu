package com.wpp.school.util

import java.util.*

/**
 * 时间util
 */
class TimeUtil {
    companion object {

        fun getWeekOfDate(date: Date?): Int {
            val weekDays = arrayOf(7, 1, 2, 3, 4, 5, 6)
            val cal = Calendar.getInstance()
            if (date != null) {
                cal.time = date
            }
            var w = cal[Calendar.DAY_OF_WEEK] - 1
            if (w < 0) w = 0
            return weekDays[w]
        }

    }
}