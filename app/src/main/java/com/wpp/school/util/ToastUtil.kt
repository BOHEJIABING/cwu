package com.wpp.school.util

import android.content.Context
import android.widget.Toast

class ToastUtil {
    companion object {

        fun showLong(context: Context, text: String) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }

        fun showShort(context: Context, text: String) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }
    }
}