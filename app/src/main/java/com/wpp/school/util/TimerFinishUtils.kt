package com.wpp.school.util

import android.annotation.SuppressLint
import android.content.Context
import android.os.CountDownTimer
import com.wpp.school.data.kv.CwuKv
import com.wpp.school.router.CwuRouter
import timber.log.Timber

/**
 * 倒计时结束返回主页
 */
class TimerFinishUtils private constructor(
    millisInFuture: Long,
    countDownInterval: Long,
    val context: Context
) :
    CountDownTimer(Future, Interval) {

    override fun onTick(time: Long) {
        Timber.d("TimeToHome: onTick $time")
    }

    override fun onFinish() {
        Timber.d("TimeToHome: onFinish")
        // 结束时不在主页则返回主页
        if (!CwuKv.checkIsHome()) {
            CwuRouter.Main.homeRecommend(context)
        }
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        var mInstance: TimerFinishUtils? = null
        private val timeToHome = CwuKv.timeToHome
        var Future = timeToHome * 60
        private const val Interval: Long = 3000
        fun getInstance(context: Context): TimerFinishUtils? {
            //单例
            if (mInstance == null) {
                synchronized(TimerFinishUtils::class.java) {
                    if (mInstance == null) {
                        mInstance = TimerFinishUtils(
                            Future,
                            Interval,
                            context
                        )
                    }
                }
            }
            return mInstance
        }
    }

}

