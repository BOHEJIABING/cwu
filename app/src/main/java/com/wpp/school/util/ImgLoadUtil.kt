package com.wpp.school.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.wpp.school.BuildConfig

/**
 * 图片加载
 */
class ImgLoadUtil {
    companion object {
        /**
         * 简单加载
         */
        fun simpleLoadImg(context: View, url: String, view: ImageView) {
            Glide.with(context).load(handleUrl(url))
                .into(view)
        }

        fun simpleLoadImg(context: Context, url: String, view: ImageView) {
            Glide.with(context).load(handleUrl(url))
                .into(view)
        }

        /**
         * 设置背景
         */
        fun maxLoadImg(context: Context, url: String, view: View) {
            Glide.with(context).asBitmap().load(handleUrl(url))
                .into(object : SimpleTarget<Bitmap?>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                    ) {
                        val drawable: Drawable = BitmapDrawable(resource)
                        view.background = drawable
                    }
                })
        }

        /**
         * 设置scale
         */
        fun scaleLoadImg(
            context: Context,
            url: String,
            view: ImageView,
            scaleType: ImageView.ScaleType
        ) {
            Glide.with(context).asBitmap()
                .load(handleUrl(url))
                .into(object : SimpleTarget<Bitmap?>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                    ) {
                        view.scaleType = scaleType
                        view.setImageBitmap(resource)
                    }
                })
        }

        private fun handleUrl(url: String): String = BuildConfig.BASE_IMG_URL + url
    }

}