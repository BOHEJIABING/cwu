package com.wpp.school.ui.practice

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.data.kv.CwuKv
import kotlinx.coroutines.launch

class PracticeViewModel(private val cwuRepository: ICwuRepository) : ViewModel() {

    private val _practiceListLiveData by lazy { MutableLiveData<MutableList<BaseImg>>() }
    val practiceListLiveData: LiveData<MutableList<BaseImg>> = _practiceListLiveData

    // 获取教学实践列表
    fun getPractice() = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseImg>? = cwuRepository.getPractice().data
            list?.let { _practiceListLiveData.value = list }
        }
    }

    // 从kv里面取
    fun getLoopTime() = CwuKv.loopTime
}