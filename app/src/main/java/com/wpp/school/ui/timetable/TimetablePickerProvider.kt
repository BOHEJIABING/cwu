package com.wpp.school.ui.timetable

import com.github.gzuliyujiang.wheelpicker.contract.LinkageProvider
import com.tencent.mmkv.MMKV
import com.wpp.school.data.entity.base.BaseConfig

/**
 * 选择器配置
 */
class TimetablePickerProvider(
    private val whoSelect: Int,
    private val list: MutableList<BaseConfig>
) : LinkageProvider {
    companion object {

        const val STU_TIMETABLE = 40
        const val TEACH_TIMETABLE = 41
        const val ROOM_TIMETABLE = 42

    }

    private lateinit var stuList: MutableList<*>
    private lateinit var teacherList: MutableList<*>
    private lateinit var roomList: MutableList<*>
    private val kv = MMKV.defaultMMKV()

    override fun firstLevelVisible(): Boolean {
        return true
    }

    override fun thirdLevelVisible(): Boolean {
        return false
    }

    override fun provideFirstData(): MutableList<*> {
        loadList()
        return when (whoSelect) {
            STU_TIMETABLE   -> stuList
            TEACH_TIMETABLE -> teacherList
            ROOM_TIMETABLE  -> roomList
            else            -> ArrayList<String>()
        }
    }

    private fun loadList() {
        // 返回该模块的第一层级列表，如2018级，2019级，2021级，2022级
        stuList     = list.filter { it.module == STU_TIMETABLE }.map { it.mainConfig }.toSet().toMutableList()
        teacherList = list.filter { it.module == TEACH_TIMETABLE }.map { it.mainConfig }.toSet().toMutableList()
        roomList    = list.filter { it.module == ROOM_TIMETABLE }.map { it.mainConfig }.toSet().toMutableList()

        // 存入kv，key：第一层级+第二层级，value：层级id
        list.forEachIndexed { _, baseConfig ->
            kv.encode(baseConfig.mainConfig + baseConfig.spareConfig, baseConfig.id!!)
        }
    }

    // 获取第一层级下的第二层级
    override fun linkageSecondData(firstIndex: Int): MutableList<*> {
        return when (whoSelect) {
            STU_TIMETABLE -> {
                // firstIndex == 0时，所筛选的为2018级下的所有第二层级
                list.filter { it.mainConfig == stuList[firstIndex] }.map { it.spareConfig }.toMutableList()
            }
            TEACH_TIMETABLE -> {
                list.filter { it.mainConfig == teacherList[firstIndex] }.map { it.spareConfig }.toMutableList()
            }
            ROOM_TIMETABLE -> {
                list.filter { it.mainConfig == roomList[firstIndex] }.map { it.spareConfig }.toMutableList()
            }
            else -> ArrayList<String>()
        }
    }

    override fun linkageThirdData(firstIndex: Int, secondIndex: Int): MutableList<*> {
        return ArrayList<String>()
    }

    override fun findFirstIndex(firstValue: Any?): Int {
        return 0
    }

    override fun findSecondIndex(firstIndex: Int, secondValue: Any?): Int {
        return 0
    }

    override fun findThirdIndex(firstIndex: Int, secondIndex: Int, thirdValue: Any?): Int {
        return 0
    }
}