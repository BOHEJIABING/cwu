package com.wpp.school.ui.introduce

import android.widget.ImageView
import com.wpp.school.databinding.FragmentCollegeIntroBinding
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.main.MainActivity
import com.wpp.school.util.ImgLoadUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * 学院简介
 */
class CollegeIntroFragment : BaseFragment<FragmentCollegeIntroBinding>() {

    private val viewModel: CollegeIntroViewModel by viewModel()

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }

    override fun initViews() {
        viewModel.introImgLiveData.observe(viewLifecycleOwner) {
            // 设置背景图片
            it.spareConfig?.let { spareConfig ->
                ImgLoadUtil.maxLoadImg(
                    context = requireContext(),
                    url = spareConfig,
                    view = binding.clBg
                )
            }
            // 设置介绍图片
            ImgLoadUtil.scaleLoadImg(
                context = requireContext(),
                url = it.mainConfig,
                view = binding.ivIntro,
                scaleType = ImageView.ScaleType.FIT_START
            )
            (activity as MainActivity).hideLoading()
        }

    }

    override fun initTitleBar() {
    }

    override fun initData() {
        viewModel.getIntroduce()
    }
}

