package com.wpp.school.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.data.kv.CwuKv
import kotlinx.coroutines.launch

class HomeViewModel(private val cwuRepository: ICwuRepository) : ViewModel() {

    private val _recommendListLiveData by lazy { MutableLiveData<MutableList<BaseImg>>() }
    val recommendListLiveData: LiveData<MutableList<BaseImg>> = _recommendListLiveData

    // 获取推荐列表
    fun getMainRecommend() = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseImg>? = cwuRepository.getMainRecommend().data
            list?.let { _recommendListLiveData.value = list }
        }
    }

    // 获取无操作返回首页时间
    fun getTimeToHome() = viewModelScope.launch {
        kotlin.runCatching {
            var timeToHome = cwuRepository.getTimeToHome().data?.toInt()
            if (timeToHome == null || timeToHome <= 0) {
                timeToHome = 10
            }
            CwuKv.timeToHome = (timeToHome * 1000).toLong()
        }

    }

    // 获取轮播时间存入kv
    fun getLoopTimeFromWeb() = viewModelScope.launch {
        kotlin.runCatching {
            var loopTime = cwuRepository.getLoopTime().data?.toInt()
            if (loopTime == null || loopTime <= 0) {
                loopTime = 5
            }
            CwuKv.loopTime = (loopTime * 1000).toLong()
        }
    }

    fun getLoopTime() = CwuKv.loopTime

}