package com.wpp.school.ui.enjoy

import android.view.View
import androidx.navigation.fragment.navArgs
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.databinding.FragmentEnjoyBinding
import com.wpp.school.router.CwuRouter
import com.wpp.school.ui.base.BaseBannerAdapter
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.enjoy.EnjoyViewModel.Companion.ENJOY_MAIN
import com.wpp.school.ui.enjoy.EnjoyViewModel.Companion.ENJOY_STUDENT
import com.wpp.school.ui.enjoy.EnjoyViewModel.Companion.ENJOY_TEACHER
import com.wpp.school.ui.main.MainActivity
import com.wpp.school.ui.timetable.TimetableFragmentArgs
import com.wpp.school.util.ImgLoadUtil
import timber.log.Timber
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 师生活动
 */
class EnjoyFragment : BaseFragment<FragmentEnjoyBinding>() {

    private val args by navArgs<TimetableFragmentArgs>()
    private var whoSelect: Int = 0
    private val viewModel: EnjoyViewModel by viewModel()

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }
    override fun initViews() {
        // 背景图livedata监听
        viewModel.enjoyBackgroundLiveData.observe(viewLifecycleOwner) {
            ImgLoadUtil.maxLoadImg(
                context = requireContext(),
                url = it.mainConfig,
                view = binding.clEnjoy
            )
            (activity as MainActivity).hideLoading()
        }
        // 轮播图livedata监听
        viewModel.enjoyListLiveData.observe(viewLifecycleOwner) {
            binding.banner.visibility = View.VISIBLE
            setBanner(it)
        }
    }

    // 设置banner
    private fun setBanner(imgList: MutableList<BaseImg>) {
        val adapter = BaseBannerAdapter(imgList)
        binding.banner.setAdapter(adapter)
            .setLoopTime(viewModel.getLoopTime())             // 设置轮播时间
            .addBannerLifecycleObserver(this) // 添加生命周期观察者
            .setOnBannerListener { data, _ ->
                CwuRouter.Main.baseDetail(this, (data as BaseImg).otherInfo)
            }
        (activity as MainActivity).hideLoading()
    }

    override fun initTitleBar() {
    }

    override fun initArgs() {
        whoSelect = args.whoSelect.toInt()
    }

    override fun initData() {
        when (whoSelect) {
            ENJOY_MAIN -> {
                viewModel.getEnjoyBackground()
            }
            ENJOY_STUDENT -> {
                viewModel.getEnjoyStu()
            }
            ENJOY_TEACHER -> {
                viewModel.getEnjoyTeacher()
            }
        }
    }
}