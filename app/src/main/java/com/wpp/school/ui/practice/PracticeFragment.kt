package com.wpp.school.ui.practice

import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.databinding.FragmentTeachPracticeBinding
import com.wpp.school.router.CwuRouter
import com.wpp.school.ui.base.BaseBannerAdapter
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 教学实践
 */
class PracticeFragment : BaseFragment<FragmentTeachPracticeBinding>() {

    private val viewModel: PracticeViewModel by viewModel()

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }

    override fun initViews() {
        viewModel.practiceListLiveData.observe(viewLifecycleOwner) {
            initBanner(it)
        }
    }

    // banner初始化
    private fun initBanner(it: MutableList<BaseImg>) {
        //自定义的图片适配器，也可以使用默认的BannerImageAdapter
        val adapter = BaseBannerAdapter(it)
        binding.banner.setAdapter(adapter)
            .setLoopTime(viewModel.getLoopTime())             // 设置轮播时间
            .addBannerLifecycleObserver(this) // 添加生命周期观察者
            .setOnBannerListener { data, _ ->
                CwuRouter.Main.baseDetail(this, (data as BaseImg).otherInfo)
            }
        (activity as MainActivity).hideLoading()
    }

    override fun initTitleBar() {
    }

    override fun initData() {
        viewModel.getPractice()
    }
}