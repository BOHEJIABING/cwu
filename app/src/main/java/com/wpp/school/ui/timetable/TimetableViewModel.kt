package com.wpp.school.ui.timetable

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.tencent.mmkv.MMKV
import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.data.entity.node.TimeNode
import com.wpp.school.util.TimeUtil
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class TimetableViewModel(private val cwuRepository: ICwuRepository) : ViewModel() {

    private val _timetableListLiveData by lazy { MutableLiveData<List<BaseExpandNode>>() }
    val timetableListLiveData: LiveData<List<BaseExpandNode>> = _timetableListLiveData

    private val _conditionListLiveData by lazy { MutableLiveData<MutableList<BaseConfig>>() }
    val conditionListLiveData: LiveData<MutableList<BaseConfig>> = _conditionListLiveData

    // 获取所有层级
    fun getAllCondition() = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseConfig>? = cwuRepository.getAllCondition().data
            list?.let { _conditionListLiveData.value = list }
        }
    }

    // 根据cId获取课表
    fun getTimetableByCId(conditionId: Int) = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseImg>? = cwuRepository.getTimetableByCId(conditionId).data
            val timetables: MutableList<BaseExpandNode> = ArrayList()
            list?.forEach {
                val timeNode = TimeNode(null, it)
                timetables.add(timeNode)
            }
            _timetableListLiveData.value = findDayOfWeek(timetables)
        }
    }

    fun getConditionId(first: String, second: String) {
        val kv = MMKV.defaultMMKV()
        getTimetableByCId(kv.decodeInt(first + second))
    }

    // 默认选中
    private fun findDayOfWeek(list: MutableList<BaseExpandNode>): MutableList<BaseExpandNode> {
        when (val today = TimeUtil.getWeekOfDate(Date())) {
            6, 7 -> list[4].isExpanded = true
            1, 2, 3, 4, 5 -> list[today - 1].isExpanded = true
        }
        return list
    }
}