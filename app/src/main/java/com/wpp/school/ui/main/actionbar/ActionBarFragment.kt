package com.wpp.school.ui.main.actionbar

import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.entity.node.BaseNode
import com.wpp.school.R
import com.wpp.school.data.entity.node.FirstNode
import com.wpp.school.data.entity.node.SecondNode
import com.wpp.school.databinding.FragmentActionBarBinding
import com.wpp.school.router.CwuRouter
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.enjoy.EnjoyViewModel.Companion.ENJOY_MAIN
import com.wpp.school.ui.enjoy.EnjoyViewModel.Companion.ENJOY_STUDENT
import com.wpp.school.ui.enjoy.EnjoyViewModel.Companion.ENJOY_TEACHER
import com.wpp.school.ui.timetable.TimetablePickerProvider.Companion.ROOM_TIMETABLE
import com.wpp.school.ui.timetable.TimetablePickerProvider.Companion.STU_TIMETABLE
import com.wpp.school.ui.timetable.TimetablePickerProvider.Companion.TEACH_TIMETABLE
import timber.log.Timber

class ActionBarFragment : BaseFragment<FragmentActionBarBinding>() {

    override fun initViews() {
        binding.rvActionBar.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this.activity)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.rvActionBar.layoutManager = linearLayoutManager
        val adapter = NodeTreeAdapter()
        binding.rvActionBar.adapter = adapter
        adapter.setList(getEntity())

    }

    override fun initTitleBar() {
    }

    override fun initData() {

    }

    private fun getEntity(): List<BaseNode> {
        val list: MutableList<BaseNode> = ArrayList()

        val firstModule = FirstNode(
            null,
            R.mipmap.ic_module1,
            R.mipmap.ic_module1_selected
        ) {
            printInfo("module 1")
            CwuRouter.Intro.collegeIntro(this)
        }
        val secondModule = FirstNode(
            null,
            R.mipmap.ic_module2,
            R.mipmap.ic_module2_selected
        ) {
            printInfo("module 2")
            CwuRouter.News.collegeNews(this)
        }
        val thirdModule = FirstNode(
            null,
            R.mipmap.ic_module3,
            R.mipmap.ic_module3_selected
        ) {
            printInfo("module 3")
            CwuRouter.Practice.teachPractice(this)
        }

        val secondNodeListOne: MutableList<BaseNode> = ArrayList()
        val thirdModuleOne = SecondNode(
            null,
            R.mipmap.ic_module4_2,
            R.mipmap.ic_module4_2_selected,
            { CwuRouter.Timetable.selectTimetable(this, STU_TIMETABLE) }
        ) {
            printInfo("module 4-2")
            CwuRouter.Timetable.selectTimetable(this, TEACH_TIMETABLE)
        }

        val thirdModuleTwo = SecondNode(
            null,
            R.mipmap.ic_module4_1,
            R.mipmap.ic_module4_1_selected,
            { CwuRouter.Timetable.selectTimetable(this, STU_TIMETABLE) }
        ) {
            printInfo("module 4-1")
            CwuRouter.Timetable.selectTimetable(this, ROOM_TIMETABLE)
        }
        secondNodeListOne.add(thirdModuleOne)
        secondNodeListOne.add(thirdModuleTwo)
        val fourthModule = FirstNode(
            secondNodeListOne,
            R.mipmap.ic_module4_0,
            R.mipmap.ic_module4_0_selected
        ) {
            printInfo("module 4-0")
            CwuRouter.Timetable.selectTimetable(this, STU_TIMETABLE)
        }

        val secondNodeListTwo: MutableList<BaseNode> = ArrayList()
        val fourthModuleOne = SecondNode(
            null,
            R.mipmap.ic_module5_1,
            R.mipmap.ic_module5_1_selected,
            { CwuRouter.Enjoy.enjoy(this, ENJOY_MAIN) }
        ) {
            printInfo("module 5-1")
            CwuRouter.Enjoy.enjoy(this, ENJOY_STUDENT)
        }
        val fourthModuleTwo = SecondNode(
            null,
            R.mipmap.ic_module5_2,
            R.mipmap.ic_module5_2_selected,
            { CwuRouter.Enjoy.enjoy(this, ENJOY_MAIN) }
        ) {
            printInfo("module 5-1")
            CwuRouter.Enjoy.enjoy(this, ENJOY_TEACHER)
        }
        secondNodeListTwo.add(fourthModuleOne)
        secondNodeListTwo.add(fourthModuleTwo)
        val fifthModule = FirstNode(
            secondNodeListTwo,
            R.mipmap.ic_module5_0,
            R.mipmap.ic_module5_0_selected
        ) {
            printInfo("module 5-0")
            CwuRouter.Enjoy.enjoy(this, ENJOY_MAIN)
        }


        list.add(firstModule)
        list.add(secondModule)
        list.add(thirdModule)
        list.add(fourthModule)
        list.add(fifthModule)
        return list
    }

    private fun printInfo(string: String) {
        Timber.e(string)
    }

}