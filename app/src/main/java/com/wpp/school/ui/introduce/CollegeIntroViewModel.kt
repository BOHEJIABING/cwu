package com.wpp.school.ui.introduce

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg
import kotlinx.coroutines.launch
import timber.log.Timber

class CollegeIntroViewModel(private val cwuRepository: ICwuRepository) : ViewModel() {

    private val _introImgLiveData by lazy { MutableLiveData<BaseConfig>() }
    val introImgLiveData: LiveData<BaseConfig> = _introImgLiveData

    // 获取背景及介绍图片
    fun getIntroduce() = viewModelScope.launch {
        kotlin.runCatching {
            val introImg = cwuRepository.getIntroduce().data
            if (introImg != null) {
                _introImgLiveData.value = introImg
            }
        }
    }

}