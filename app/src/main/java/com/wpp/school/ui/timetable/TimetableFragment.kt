package com.wpp.school.ui.timetable

import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.gzuliyujiang.wheelpicker.contract.OnLinkagePickedListener
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.databinding.FragmentTimetableBinding
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class TimetableFragment : BaseFragment<FragmentTimetableBinding>(), OnLinkagePickedListener {

    private val viewModel: TimetableViewModel by viewModel()

    private val args by navArgs<TimetableFragmentArgs>()
    private var whoSelect: Int = 0

    override fun onLinkagePicked(first: Any?, second: Any?, third: Any?) {}

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }

    override fun initViews() {
        viewModel.conditionListLiveData.observe(viewLifecycleOwner) { list ->
            initPicker(list)
            list.filter { it.module == whoSelect }.toMutableList()[0].id?.let { id ->
                // 获取列表第一项（之前面层级）的课表
                viewModel.getTimetableByCId(id)
            }
            // 选择器选择发生改变时的回调
            binding.wlPicker.setOnLinkageSelectedListener { first, second, _ ->
                // 根据层级获取id，并显示课表
                viewModel.getConditionId(first.toString(), second.toString())
            }
        }
        // 课表图片返回后初始化TimetableAdapter
        viewModel.timetableListLiveData.observe(viewLifecycleOwner) {
            val adapter = TimetableAdapter()
            binding.rvTimetable.adapter = adapter
            adapter.setList(it)
            (activity as MainActivity).hideLoading()
        }
    }

    // 初始化选择器
    private fun initPicker(list: MutableList<BaseConfig>) {
        binding.wlPicker.setData(TimetablePickerProvider(whoSelect, list))
        val linearLayoutManager = LinearLayoutManager(this.activity)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.rvTimetable.layoutManager = linearLayoutManager
    }


    override fun initTitleBar() {
    }

    override fun initArgs() {
        whoSelect = args.whoSelect.toInt()
    }

    override fun initData() {
        // 获取层级
        viewModel.getAllCondition()
    }
}