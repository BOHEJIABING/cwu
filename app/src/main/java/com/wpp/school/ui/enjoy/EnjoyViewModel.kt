package com.wpp.school.ui.enjoy

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.entity.base.BaseConfig
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.data.kv.CwuKv
import kotlinx.coroutines.launch

class EnjoyViewModel(private val cwuRepository: ICwuRepository) : ViewModel() {
    companion object {
        const val ENJOY_MAIN = 0
        const val ENJOY_STUDENT = 1
        const val ENJOY_TEACHER = 2
    }
    private val _enjoyListLiveData by lazy { MutableLiveData<MutableList<BaseImg>>() }
    val enjoyListLiveData: LiveData<MutableList<BaseImg>> = _enjoyListLiveData

    private val _enjoyBackgroundLiveData by lazy { MutableLiveData<BaseConfig>() }
    val enjoyBackgroundLiveData: LiveData<BaseConfig> = _enjoyBackgroundLiveData

    // 请求背景图
    fun getEnjoyBackground() = viewModelScope.launch {
        kotlin.runCatching {
            val baseConfig = cwuRepository.getEnjoyBackground().data
            baseConfig?.let { _enjoyBackgroundLiveData.value = baseConfig }
        }
    }

    // 请求学生活动图组
    fun getEnjoyStu() = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseImg>? = cwuRepository.getEnjoyStu().data
            list?.let { _enjoyListLiveData.value = list }
        }
    }

    // 请求教师活动图组
    fun getEnjoyTeacher() = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseImg>? = cwuRepository.getEnjoyTeacher().data
            list?.let { _enjoyListLiveData.value = list }
        }
    }

    // 请求轮播时间
    fun getLoopTime() = CwuKv.loopTime
}