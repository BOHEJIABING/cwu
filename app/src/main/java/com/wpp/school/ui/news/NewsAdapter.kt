package com.wpp.school.ui.news

import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wpp.school.R
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.util.ImgLoadUtil

/**
 * newsAdapter
 */
class NewsAdapter(data: MutableList<BaseImg>?) :
    BaseQuickAdapter<BaseImg, BaseViewHolder>(R.layout.item_news, data) {

    override fun convert(holder: BaseViewHolder, item: BaseImg) {
        ImgLoadUtil.scaleLoadImg(
            context = context,
            url = item.url,
            view = holder.getView(R.id.iv_news) as ImageView,
            scaleType = ImageView.ScaleType.CENTER_CROP
        )

    }

}

