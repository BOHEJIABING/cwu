package com.wpp.school.ui.timetable

import android.view.View
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.provider.BaseNodeProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wpp.school.R
import com.wpp.school.data.entity.node.TimeNode
import com.wpp.school.util.ImgLoadUtil

/**
 * 课表node配置
 */
open class TimetableNodeProvider : BaseNodeProvider() {
    override val itemViewType: Int
        get() = 1
    override val layoutId: Int
        get() = R.layout.item_timetable

    override fun convert(helper: BaseViewHolder, item: BaseNode) {
        val entity = item as TimeNode
        ImgLoadUtil.simpleLoadImg(
            context = context,
            url = entity.baseImg.url,
            view = helper.getView(R.id.iv_timetable)
        )
        setReplaceImg(helper, item)
    }

    private fun setReplaceImg(helper: BaseViewHolder, item: BaseNode) {
        val entity = item as TimeNode
        ImgLoadUtil.simpleLoadImg(
            context = context,
            url = if (entity.isExpanded) entity.baseImg.otherInfo else entity.baseImg.url,
            view = helper.getView(R.id.iv_timetable)
        )
    }

    // 点击事件
    override fun onClick(helper: BaseViewHolder, view: View, data: BaseNode, position: Int) {
        if ((data as TimeNode).isExpanded) {
            getAdapter()!!.collapse(position, notify = true)
        } else {
            getAdapter()!!.expand(position, notify = true)
            expandAllOtherNode(position)
        }
    }

    private fun expandAllOtherNode(position: Int) {
        val parentPosition = getAdapter()!!.findParentNode(position)
        // 当前层级顶部开始位置
        val firstPosition: Int = if (parentPosition == -1) {
            0 // 如果没有父节点，则为最外层，从0开始
        } else {
            parentPosition + 1 // 如果有父节点，则为子节点，从 父节点+1 的位置开始
        }
        // 当前 position 之前有 node 收起以后，position的位置就会变化
        var newPosition = position
        // 在此之前的 node 总数
        val beforeAllSize = position - firstPosition
        // 如果此 position 之前有 node
        if (beforeAllSize > 0) {
            // 从顶部开始位置循环
            var i = firstPosition
            do {
                val collapseSize = getAdapter()!!.collapse(i)
                i++
                // 每次折叠后，重新计算新的 Position
                newPosition -= collapseSize
            } while (i < newPosition)
        }
        val expandCount = 0
        // 当前层级最后的位置
        var lastPosition: Int = if (parentPosition == -1) {
            getAdapter()!!.data.size - 1 // 如果没有父节点，则为最外层
        } else {
            val dataSize = getAdapter()!!.data[parentPosition].childNode?.size ?: 0
            parentPosition + dataSize + expandCount // 如果有父节点，则为子节点，父节点 + 子节点数量 + 展开的数量
        }

        //如果此 position 之后有 node
        if ((newPosition + expandCount) < lastPosition) {
            var i = newPosition + expandCount + 1
            while (i <= lastPosition) {
                val collapseSize = getAdapter()!!.collapse(i)
                i++
                lastPosition -= collapseSize
            }

        }
    }
}