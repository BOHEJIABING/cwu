package com.wpp.school.ui.splash

import androidx.constraintlayout.widget.ConstraintLayout
import com.wpp.school.R
import com.wpp.school.databinding.FragmentSplashBinding
import com.wpp.school.ui.base.BaseFragment

/**
 * 闪屏页
 */
class SplashFragment : BaseFragment<FragmentSplashBinding>(){

    override fun initData() {

    }

    override fun initViews() {
        activity?.findViewById<ConstraintLayout>(R.id.mainActivityLayout)?.background = null
    }

    override fun initTitleBar() {
    }

}