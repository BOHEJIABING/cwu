package com.wpp.school.ui.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wpp.school.data.ICwuRepository
import com.wpp.school.data.entity.base.BaseImg
import kotlinx.coroutines.launch

class NewsViewModel(private val cwuRepository: ICwuRepository) : ViewModel() {

    private val _newsListLiveData by lazy { MutableLiveData<MutableList<BaseImg>>() }
    val newsListLiveData: LiveData<MutableList<BaseImg>> = _newsListLiveData

    // 获取新闻列表
    fun getNews() = viewModelScope.launch {
        kotlin.runCatching {
            val list: MutableList<BaseImg>? = cwuRepository.getNews().data
            list?.let { _newsListLiveData.value = list }
        }
    }

}
