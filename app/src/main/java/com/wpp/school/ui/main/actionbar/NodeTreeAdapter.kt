package com.wpp.school.ui.main.actionbar

import com.chad.library.adapter.base.BaseNodeAdapter
import com.chad.library.adapter.base.entity.node.BaseNode
import com.wpp.school.data.entity.node.FirstNode
import com.wpp.school.data.entity.node.SecondNode


class NodeTreeAdapter: BaseNodeAdapter() {

    init {
        addFooterNodeProvider(NodeProvider())
    }

    /**
     * 自行根据数据、位置等信息，返回 item 类型
     */
    override fun getItemType(data: List<BaseNode>, position: Int): Int {
        return when (data[position]) {
            is FirstNode -> {
                1
            }
            is SecondNode -> {
                2
            }
            else -> -1
        }
    }
}