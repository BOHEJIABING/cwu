package com.wpp.school.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.dylanc.viewbinding.base.inflateBindingWithGeneric
import com.wpp.school.data.kv.CwuKv
import timber.log.Timber

/**
 * baseFragment基类
 */
abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    private var _binding: VB? = null
    val binding: VB get() = _binding!!

    protected val fragmentTag: String = javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = inflateBindingWithGeneric(layoutInflater, container, false)
        initArgs()
        initPage()
        return binding.root
    }

    protected open fun initPage() {
        initTag()
        initTitleBar()
        initData()
        initViews()
        initListeners()
    }

    private fun initTag() {
        if (fragmentTag != "ActionBarFragment") {
            CwuKv.thisFragment = fragmentTag
        }
    }

    protected fun getRootView(): View {
        return binding.root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected open fun initListeners() {}

    protected abstract fun initTitleBar()

    protected open fun initArgs() {}

    protected abstract fun initData()

    protected abstract fun initViews()
}