package com.wpp.school.ui.timetable

import com.chad.library.adapter.base.BaseNodeAdapter
import com.chad.library.adapter.base.entity.node.BaseNode
import com.wpp.school.data.entity.node.TimeNode

/**
 * 课表nodeAdapter
 */
class TimetableAdapter: BaseNodeAdapter() {

    init {
        addFooterNodeProvider(TimetableNodeProvider())
    }
    override fun getItemType(data: List<BaseNode>, position: Int): Int {
        return when (data[position]) {
            is TimeNode -> 1
            else -> -1
        }
    }
}