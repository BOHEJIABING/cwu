package com.wpp.school.ui.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.wpp.school.R
import com.wpp.school.databinding.ActivityMainBinding
import com.wpp.school.router.CwuRouter
import com.wpp.school.ui.base.BaseActivity
import com.wpp.school.util.StatusBarUtil
import timber.log.Timber

/**
 * MainActivity
 */
class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        const val KEY_JUMP_ACTION = "KEY_JUMP_ACTION"

        const val JUMP_BANNER = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusBarUtil.setTransparent(this)
    }

    // FragmentManager
    fun findNavHostFragment(): NavHostFragment {
        return supportFragmentManager.fragments.find { it is NavHostFragment } as NavHostFragment
    }

    // 展示loading
    fun showLoading() {
        Timber.d("MainActivity load: showLoading")
        binding.ivLoading.visibility = View.VISIBLE
    }

    // 隐藏loading，考虑到将图片设置上去的时间，延时0.1秒隐藏
    fun hideLoading() {
        Timber.d("MainActivity load: hideLoading")
        Handler(Looper.getMainLooper()).postDelayed(
            {
                binding.ivLoading.visibility = View.GONE
            }, 100
        )
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        //重新设置一下intent，否则不杀死进程进入App会导致携带之前的数据
        setIntent(getIntent())
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent?) {
        if (intent == null) return
        when (intent.getIntExtra(KEY_JUMP_ACTION, -1)) {
            JUMP_BANNER -> {
                CwuRouter.Main.homeRecommend(findNavHostFragment())
            }
        }

    }

    override fun initListeners() {

    }

    // 顶部底部条
    override fun initViews() {
        Glide.with(this).load(R.drawable.bg_home_title).into(binding.ivSchoolTitle)
        Glide.with(this).load(R.drawable.loading).into(binding.ivLoading)
    }

    override fun initTitleBar() {
    }

    override fun initArgs() {
    }

    override fun initData() {
    }


}
