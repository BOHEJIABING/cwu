package com.wpp.school.ui.base

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.util.ImgLoadUtil
import com.youth.banner.adapter.BannerAdapter

/**
 * baseBannerAdapter 轮播图适配器基类
 */
class BaseBannerAdapter(imgList: MutableList<BaseImg>) :
    BannerAdapter<BaseImg, BannerImgHolder>(imgList) {
    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): BannerImgHolder {
        val imageView = ImageView(parent!!.context)
        //注意，必须设置为match_parent，这个是viewpager2强制要求的
        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        imageView.layoutParams = params
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        return BannerImgHolder(imageView)
    }

    override fun onBindView(holder: BannerImgHolder?, data: BaseImg?, position: Int, size: Int) {
        //图片加载
        if (data != null) {
            ImgLoadUtil.simpleLoadImg(
                context = holder!!.itemView,
                url = data.url,
                view = holder.imageView
            )
        }
    }
}

class BannerImgHolder(view: View) : RecyclerView.ViewHolder(view) {
    var imageView: ImageView = view as ImageView
}