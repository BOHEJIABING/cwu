package com.wpp.school.ui.main.actionbar

import android.view.View
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.provider.BaseNodeProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wpp.school.R
import com.wpp.school.data.entity.node.FirstNode
import com.wpp.school.data.entity.node.SecondNode
import com.wpp.school.ui.main.MainActivity
import com.wpp.school.ui.main.actionbar.NodeTreeAdapter

open class NodeProvider : BaseNodeProvider() {
    override val itemViewType: Int
        get() = 1
    override val layoutId: Int
        get() = R.layout.item_node

    override fun convert(helper: BaseViewHolder, item: BaseNode) {
        // 设置图标
        val entity: FirstNode = if (itemViewType == 1) {
            item as FirstNode
        } else {
            item as SecondNode
        }
        Glide.with(context).load(entity.originImg).into(helper.getView(R.id.iv_head))
        setReplaceImg(helper, item)
    }

    // 图标替换
    private fun setReplaceImg(helper: BaseViewHolder, item: BaseNode) {
        val entity: FirstNode = if (itemViewType == 1) {
            item as FirstNode
        } else {
            item as SecondNode
        }
        Glide.with(context).load(if (entity.isExpanded) entity.replaceImg else entity.originImg)
            .into(helper.getView(R.id.iv_head))
    }

    // 点击事件
    override fun onClick(helper: BaseViewHolder, view: View, data: BaseNode, position: Int) {
        var entity = data as FirstNode
        if (data.isExpanded) {
            // 当前处于展开状态，点击时收起
            getAdapter()!!.collapse(position, notify = true)
            // 收起事件
            if (getAdapter()!!.findParentNode(data) == -1) {
                // 不存在父节点时，收回直接返回主页
                (context as MainActivity).findNavHostFragment().findNavController()
                    .popBackStack(R.id.home, false)
            } else {
                // 存在父节点时，展示父节点
                entity = entity as SecondNode
                entity.methodBack?.invoke()
            }
        } else {
            // 当前处于收起状态，点击时展开
            entity.method.invoke()
            getAdapter()!!.expand(position, notify = true)
            // 展开时收起其余所有处于展开状态的node
            expandAllOtherNode(position)
        }
    }

    /**
     * 使其他所有的节点收起，不管是否含有子节点，参考expandAndCollapseOther
     */
    private fun expandAllOtherNode(position: Int) {
        val parentPosition = getAdapter()!!.findParentNode(position)
        // 当前层级顶部开始位置
        val firstPosition: Int = if (parentPosition == -1) {
            0 // 如果没有父节点，则为最外层，从0开始
        } else {
            parentPosition + 1 // 如果有父节点，则为子节点，从 父节点+1 的位置开始
        }
        // 当前 position 之前有 node 收起以后，position的位置就会变化
        var newPosition = position
        // 在此之前的 node 总数
        val beforeAllSize = position - firstPosition
        // 如果此 position 之前有 node
        if (beforeAllSize > 0) {
            // 从顶部开始位置循环
            var i = firstPosition
            do {
                val collapseSize = getAdapter()!!.collapse(i)
                i++
                // 每次折叠后，重新计算新的 Position
                newPosition -= collapseSize
            } while (i < newPosition)
        }
        val expandCount = 0
        // 当前层级最后的位置
        var lastPosition: Int = if (parentPosition == -1) {
            getAdapter()!!.data.size - 1 // 如果没有父节点，则为最外层
        } else {
            val dataSize = getAdapter()!!.data[parentPosition].childNode?.size ?: 0
            parentPosition + dataSize + expandCount // 如果有父节点，则为子节点，父节点 + 子节点数量 + 展开的数量
        }

        //如果此 position 之后有 node
        if ((newPosition + expandCount) < lastPosition) {
            var i = newPosition + expandCount + 1
            while (i <= lastPosition) {
                val collapseSize = getAdapter()!!.collapse(i)
                i++
                lastPosition -= collapseSize
            }

        }
    }
}