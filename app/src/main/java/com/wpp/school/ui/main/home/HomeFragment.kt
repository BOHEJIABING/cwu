package com.wpp.school.ui.main.home

import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.databinding.FragmentHomeBinding
import com.wpp.school.router.CwuRouter
import com.wpp.school.ui.base.BaseBannerAdapter
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 推荐列表
 */
class HomeFragment : BaseFragment<FragmentHomeBinding>() {


    private val viewModel: HomeViewModel by viewModel()

    override fun initData() {
        // 请求轮播间隔时间
        viewModel.getLoopTimeFromWeb()

        // 请求推荐列表
        viewModel.getMainRecommend()

        // 请求返回主页时间
        viewModel.getTimeToHome()
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }

    override fun initViews() {
        //自定义的图片适配器，也可以使用默认的BannerImageAdapter
        viewModel.recommendListLiveData.observe(viewLifecycleOwner) {
            initBanner(it)
            (activity as MainActivity).hideLoading()
        }
    }

    /**
     * 初始化banner，设置数据及参数
     */
    private fun initBanner(it: MutableList<BaseImg>) {
        val adapter = BaseBannerAdapter(it)
        binding.banner.setAdapter(adapter)
            .setLoopTime(viewModel.getLoopTime())             // 设置轮播时间
            .addBannerLifecycleObserver(this) // 添加生命周期观察者
            .setOnBannerListener { data, _ ->
                CwuRouter.Main.baseDetail(this, (data as BaseImg).otherInfo)
            }

    }


    override fun initTitleBar() {

    }

}
