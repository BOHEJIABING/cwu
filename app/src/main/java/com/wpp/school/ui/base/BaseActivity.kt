package com.wpp.school.ui.base

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.dylanc.viewbinding.base.inflateBindingWithGeneric
import com.wpp.school.data.kv.CwuKv
import com.wpp.school.util.TimerFinishUtils

/**
 * activity基类
 */
abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    lateinit var binding: VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = inflateBindingWithGeneric(layoutInflater)
        initArgs()
        initPage()
        setContentView(binding.root)
    }

    protected open fun initPage() {
        initData()
        initViews()
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        if (!CwuKv.checkIsHome()) {
            TimerFinishUtils.getInstance(this)!!.start()
        }
    }

    override fun onPause() {
        super.onPause()
        TimerFinishUtils.getInstance(this)!!.cancel()
    }

    // 无操作返回主页
    override fun onUserInteraction() {
        TimerFinishUtils.getInstance(this)!!.cancel()
        // (用户开始操作 && APP不处于首页 && APP不处于播放状态) 重新计时
        // 延时计时：点击跳转时fragment的onCreateView 在 onUserInteraction之后
        Handler(Looper.getMainLooper()).postDelayed({
            if (!CwuKv.checkIsHome() && !CwuKv.isVideoPlay) {
                TimerFinishUtils.getInstance(this)!!.start()
            }
        }, 500)
        super.onUserInteraction()
    }

    protected abstract fun initListeners()

    protected abstract fun initViews()

    protected abstract fun initTitleBar()

    protected abstract fun initArgs()

    abstract fun initData()
}