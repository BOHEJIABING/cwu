package com.wpp.school.ui.base

import android.view.View
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.shuyu.gsyvideoplayer.GSYVideoManager
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack
import com.wpp.school.BuildConfig
import com.wpp.school.data.kv.CwuKv
import com.wpp.school.databinding.FragmentBaseDetailBinding
import com.wpp.school.ui.main.MainActivity
import com.wpp.school.util.ImgLoadUtil
import com.wpp.school.util.TimerFinishUtils
import timber.log.Timber


class BaseDetailFragment : BaseFragment<FragmentBaseDetailBinding>() {

    private val args by navArgs<BaseDetailFragmentArgs>()
    private var detailUrl: String = ""

    override fun initListeners() {
        binding.clBaseDetail.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }

    override fun initViews() {
        if (detailUrl.endsWith(".mp4")) {
            videoInit()
        } else {
            imgInit()
        }
    }

    private fun imgInit() {
        binding.detailImg.visibility = View.VISIBLE
        ImgLoadUtil.scaleLoadImg(
            context = requireContext(),
            url = detailUrl,
            view = binding.detailImg,
            scaleType = ImageView.ScaleType.CENTER_CROP
        )
        (activity as MainActivity).hideLoading()
    }

    private fun videoInit() {
        val videoUrl = BuildConfig.BASE_VIDEO_URL + detailUrl
        Timber.d("$fragmentTag: $videoUrl")
        (activity as MainActivity).hideLoading()
        activity?.let { TimerFinishUtils.getInstance(it) }!!.cancel()
        binding.detailVideo.apply {
            visibility = View.VISIBLE
            setUp(videoUrl, true, "")
            setIsTouchWiget(true) //是否可以滑动调整
            backButton.setOnClickListener { onBackPressed() }// 设置返回按键功能
            isShowFullAnimation = false    // 不需要动画
            isNeedOrientationUtils = false  ///不需要屏幕旋转
            fullscreenButton.setOnClickListener {
                startWindowFullscreen(context, false, true)
            }
            setVideoAllCallBack(object : GSYSampleCallBack() {
                override fun onClickResumeFullscreen(url: String?, vararg objects: Any?) {
                    Timber.d("$fragmentTag TimeToHome: video resume")
                    CwuKv.isVideoPlay = true
                    super.onClickResumeFullscreen(url, *objects)
                }

                override fun onClickStopFullscreen(url: String?, vararg objects: Any?) {
                    Timber.d("$fragmentTag TimeToHome: video stop")
                    CwuKv.isVideoPlay = false
                    super.onClickStopFullscreen(url, *objects)
                }

                override fun onAutoComplete(url: String?, vararg objects: Any?) {
                    Timber.d("$fragmentTag TimeToHome: video finish")
                    CwuKv.isVideoPlay = false
                    activity?.let { TimerFinishUtils.getInstance(it) }!!.start()
                    super.onAutoComplete(url, *objects)
                }

                override fun onTouchScreenSeekVolume(url: String?, vararg objects: Any?) {
                    kotlin.runCatching {
                        super.onTouchScreenSeekVolume(url, *objects)
                    }
                }

                override fun onTouchScreenSeekPosition(url: String?, vararg objects: Any?) {
                    kotlin.runCatching {
                        super.onTouchScreenSeekPosition(url, *objects)
                    }
                }

                override fun onTouchScreenSeekLight(url: String?, vararg objects: Any?) {}
            })
            startPlayLogic()
            startWindowFullscreen(context, false, true)
        }
        CwuKv.isVideoPlay = true
    }

    private fun onBackPressed() {
        //释放所有
        binding.detailVideo.setVideoAllCallBack(null)
        activity?.let { TimerFinishUtils.getInstance(it) }!!.start()
        activity?.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        binding.detailVideo.onVideoPause()
    }

    override fun onResume() {
        super.onResume()
        binding.detailVideo.onVideoResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        CwuKv.isVideoPlay = false
        // 返回的是主页，取消计时
        activity?.let { TimerFinishUtils.getInstance(it) }!!.cancel()
        GSYVideoManager.releaseAllVideos()
    }

    override fun initTitleBar() {
    }

    override fun initArgs() {
        detailUrl = args.detailUrl
    }

    override fun initData() {

    }
}