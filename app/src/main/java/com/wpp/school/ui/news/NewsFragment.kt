package com.wpp.school.ui.news

import androidx.recyclerview.widget.LinearLayoutManager
import com.wpp.school.data.entity.base.BaseImg
import com.wpp.school.databinding.FragmentNewsBinding
import com.wpp.school.router.CwuRouter
import com.wpp.school.ui.base.BaseFragment
import com.wpp.school.ui.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * 新闻动态
 */
class NewsFragment : BaseFragment<FragmentNewsBinding>() {

    private val viewModel: NewsViewModel by viewModel()
    private lateinit var adapter: NewsAdapter
    private lateinit var newsList: MutableList<BaseImg>

    override fun initViews() {
        // newsList监听
        viewModel.newsListLiveData.observe(viewLifecycleOwner) {
            newsList = it
            initList()
        }

    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).showLoading()
    }

    // listView初始化
    private fun initList() {
        adapter = NewsAdapter(newsList)
        binding.rcNews.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this.activity)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.rcNews.layoutManager = linearLayoutManager
        binding.rcNews.adapter = adapter

        (activity as MainActivity).hideLoading()
        // 设置点击事件
        adapter.setOnItemClickListener { _, _, position ->
            CwuRouter.Main.baseDetail(this, newsList[position].otherInfo)
        }

    }

    override fun initTitleBar() {
    }

    override fun initData() {
        viewModel.getNews()
    }
}